/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import javafx.util.Pair;

/**
 *
 * @author mozar
 */
public abstract class ChessPiece {

    private int posHorizontal, posVertical;

    public ChessPiece(int posHorizontal, int posVertical) {
        this.posHorizontal = posHorizontal;
        this.posVertical = posVertical;
    }

    public void setPosition(int horizontal, int vertical) {

    }

    public int getPosHorizontal() {
        return this.posHorizontal;
    }
    
    public int getPosVertical(){
        return this.posVertical;
    }

    abstract List<Pair<Integer, Integer>> availableMove();

    abstract boolean canEatPiece(ChessPiece cp);
}
